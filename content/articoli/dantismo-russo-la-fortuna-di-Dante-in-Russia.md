---
title: "Dantismo russo. La fortuna di Dante in Russia"
date: 2023-04-24T16:35:51+02:00
description: "Se si prendono in considerazione tutte le opere ispirate alla Divina Commedia che sono state scritte in Russia tra il XIX e il XX secolo, si rimane impressionati dall’interesse che Dante Alighieri aveva suscitato tra gli intellettuali russi di quell’epoca. Di tutta questa produzione letteraria sul poeta fiorentino, colpisce il modo in cui la sua opera era stata interpretata, tanto da portare la critica letteraria a coniare una nuova espressione per descrivere questo particolare fenomeno: il dantismo russo. Reinterpretato soprattutto dai poeti simbolisti, Dante era stato di fatto trasformato in un vero e proprio “Dante russo”, che spesso poco aveva a che vedere con il sommo poeta fiorentino."
featured: true
tags:
- dante
- divina commedia
- letteratura russa
- blok
- gippius
- simbolismo
- gogol
- dantismo russo
toc: true
featured: false
image: "/img/articoli/Dante.jpg"
imageCaption: "_Dante con la Divina Commedia_ (1465) di Domenico di Michelino, conservato nel Duomo di Firenze. Fonte: <a href='https://commons.wikimedia.org/wiki/File:Dante_Domenico_di_Michelino_Duomo_Florence.jpg' target='_blank'>Wikimedia Commons</a>"
---

---

Se si prendono in considerazione tutte le opere ispirate alla _Divina Commedia_ che sono state scritte in Russia tra il XIX e il XX secolo, si rimane impressionati dall’interesse che Dante Alighieri aveva suscitato nell’_intelligencija_ russa in quell'epoca. Di tutta questa produzione letteraria sul poeta fiorentino, colpisce il modo in cui la sua opera era stata interpretata, tanto da portare la critica letteraria a coniare una nuova espressione per descrivere questo particolare fenomeno: il **dantismo russo**. Reinterpretato soprattutto dai poeti simbolisti, Dante era stato di fatto trasformato in un vero e proprio **“Dante russo”** (Colucci 1989), che spesso poco aveva a che vedere con il sommo poeta fiorentino.

## L'arrivo di Dante in Russia

Dante approda per la prima volta in Russia soltanto nel **1757**, quando l’imperatrice Elisabetta (1709-1761) riceve in regalo un’edizione veneziana della _Divina Commedia_. È da allora che iniziarono a circolarne alcune copie anche a San Pietroburgo, seguite dalla timida comparsa in riviste ed enciclopedie di alcuni articoli sul poeta Dante Alighieri, e di traduzioni in russo di qualche frammento sparso della _Commedia_ (i primi a essere tradotti furono i primi 75 versi del canto XXVIII del _Purgatorio_, nel 1798).

Nonostante sia stata tradotta integralmente in russo soltanto a partire dal **1842**, la _Divina Commedia_ era comunque abbastanza conosciuta negli ambienti culturali moscoviti e pietroburghesi, grazie alla diffusione delle **edizioni francesi**. Tra i personaggi della _Commedia_ erano particolarmente amati il Conte Ugolino e Francesca da Rimini, sui quali furono composte diverse opere teatrali e sinfonie, come ad esempio _Ugolino_ (1837) di Nikolaj Polevoj e _Francesca da Rimini_ (1878) di Pëtr Čajkovskij. Inoltre, ad Aleksandr Puškin si deve il primo esempio di poesia russa in terzine dantesche: _All’inizio della vita ricordo la scuola_ (1830). Il grande poeta russo considerava Dante un maestro della letteratura e possedeva diversi esemplari della _Divina Commedia_, tra cui una versione francese del 1596.

**Perché Dante veniva letto in francese?** Perché il francese era la lingua ufficiale dell'aristocrazia russa. Si pensi persino che molti dialoghi in _Guerra e pace_ (1865-1869) sono in francese!

## Le reinterpretazioni russe di Dante

Quando si parla di storia della traduzione russa della _Divina Commedia_, è importante tenere presente che la tradizione cui facevano riferimento i letterati russi del XIX secolo era, appunto, quella francese. La problematicità di questa constatazione risiede nel fatto che traducendo un’opera da una lingua diversa da quella di partenza, si rischia di trasferire alla traduzione un’interpretazione che appartiene a un’altra cultura, travisando di conseguenza il testo originale. Come se non bastasse, i dantisti russi avevano l'abitudine di interpretare l'opera dantesca a seconda alle mode che si susseguivano, decennio dopo decennio, in Russia.

### Dante romantico

Dalla lettura francese della _Divina Commedia_ (XIX secolo) deriva l'interpretazione romantica di Dante, il quale veniva concepito come un aristocratico, cultore del dolce stilnovo, militante per la giustizia, cantore di passioni e di terrore gotico, artefice di una lingua nazionale per il popolo italiano.

Il tema della **lingua nazionale** era inoltre molto sentito nella Russia ottocentesca, così il lavoro di Dante con il volgare fiorentino fu preso come esempio per l'introduzione di una riforma della lingua letteraria russa. Se Dante insisteva sulla dignità della lingua volgare, poeti come Aleksandr Puškin, Kostantin Batjuškov o Vasilij Žukovskij insistevano sul bisogno di una nuova lingua letteraria che si distaccasse dallo slavo ecclesiastico[^1] e, attraverso le loro poesie, crearono la lingua della letteratura classica.

Nell’ambiente decabrista[^2] Dante veniva invece considerato un eroe che si era battuto per la libertà, la verità e il popolo italiano; inoltre, tra le varie traduzioni dei canti della _Divina Commedia_, quella di **Pavel Katenin** dei primi tre canti dell'_Inferno_ (1828-1829) viene considerata un classico esempio di **poesia decabrista**, proprio per il _pathos_ civile di cui questa traduzione è permeata:

{{< mquote content="E qual è quei che volentieri acquista,<br>e giugne 'l tempo che perder lo face,<br>che 'n tutti suoi pensier piange e s'attrista;<br><br>tal mi fece la bestia sanza pace,<br>che, venendomi 'ncontro, a poco a poco<br>mi ripigneva là dove 'l sol tace."
author="_Inferno_, canto I, vv. 55-60."
translation="И каково тому, кто скопит злата,<br>Как все терять придет ему чреда:<br>Тут в мыслях плач и горькая утрата;<br><br>Таким меня зверь сотворил тогда,<br>Помалу вспять гоня к стремнине тесной,<br>Где солнца луч не светит никогда."
translationAuthor="Traduzione di Katenin"
comment="**Parafrasi delle terzine russe**: e come colui che raggruzzola dell'oro, e poi arriva il suo turno di perdere tutto: nei pensieri c'era pianto e amara perdita; così mi aveva reso quella bestia allora, cacciandomi indietro, a poco a poco, verso l'angusto declivo dove non splendono mai i raggi del sole."
>}}

### Dante teologo

Nel 1833 l'uscita del saggio _Dante e il suo secolo_ di Stepan Ševyrëv inaugura il mito del Dante teologo. Questo saggio è il primo studio russo su Dante in cui la _Divina Commedia_ viene definita un’unione tra poesia e religione: la _Commedia_ diventa così un **poema lirico-simbolico**, mentre Dante diventa un **poeta-teologo**. Quella dantesca divenne così un’opera di grande significato metafisico-spirituale che ispirò soprattutto i **poeti simbolisti**, come Z. N. Gippius, Dmitrij Merežkovskij, Aleksandr Blok, Kostantin Bal’mont, Valerij Brjusov, Vjačeslav Ivanov ed Ellis. Ciò che caratterizzava il clima culturale dell’**Età d’argento** era difatti la ricerca di una nuova unità culturale e religiosa per la Russia, e chi meglio di Dante avrebbe potuto guidare il popolo russo in un percorso esoterico che conduceva ai misteri più intimi dell’essere?

La principale fonte di tale reinterpretazione sono gli scritti di **Vladimir Solov’ëv** (1853-1900) sulla Divina Sofia. Fu soprattutto l’**_Inferno_**, con il suo viaggio di pena ed espiazione, a stimolare la fantasia dei simbolisti: secondo loro, questo viaggio di purificazione poteva essere compiuto solo da chi aveva al proprio fianco una guida e una **Dama celeste** verso la quale ascendere.

Anche i poeti simbolisti si cimentarono nella traduzione di alcuni frammenti della _Divina Commedia_ e della _Vita nova_: Valerij Brjusov, Vjačeslav Ivanov ed Ellis tradussero rispettivamente l'_Inferno_, il _Purgatorio_ e il _Paradiso_. Le loro traduzioni evidenziano non solo il loro interesse per alcuni aspetti della _Commedia_ (la pena eterna, l’esilio, la purificazione dell’anima, l’ispirazione poetica), ma anche la loro volontà di infondere nella poesia russa nuove idee, temi e immagini. Ad esempio, le traduzioni di Ivanov della _Vita nova_, del _Convivio_ e della _Commedia_ volevano trasmettere l’idea della natura simbolica della poesia, rappresentando Dante come il **poeta-teurgo**:

{{< mquote content="Dolce color d'orïental zaffiro,<br>che s'accoglieva nel sereno aspetto<br>del mezzo, puro infino al primo giro,<br><br>a li occhi miei ricominciò diletto,<br>tosto ch'io usci' fuor de l'aura morta<br>che m'avea contristati li occhi e 'l petto."
author="_Purgatorio_, canto I, vv. 13-18."
translation="Цвет сладостный восточного сафира<br>По первый круг сгущаясь в вышине<br>Чистейшего, прозрачного эфира,<br><br>Опять целил и нежил очи мне,<br>Так долго мертвым воздухом, без света,<br>Дышавшему в исхоженной стране."
translationAuthor="Traduzione di Ivanov"
comment="**Parafrasi delle terzine russe**: il dolce colore dello zaffiro orientale, che per il primo giro si propagava nelle altezze dell'etere puro e limpido, raggiunse di nuovo i miei occhi, deliziandomi, che a lungo avevo respirato l'aria morta, senza luce, nel lido dal quale venivo."
>}}

### Dante _raznočinec_

Dopo la crisi del Simbolismo nel 1910, la concezione dell’opera dantesca subì un’importante trasformazione sotto l'influenza della **poesia acmeista**. Fu soprattutto nell’opera di Anna Achmatova e di Osip Mandel’štam che venne fatta una nuova riflessione sulla **lingua della _Divina Commedia_**, che fu presa a modello per l’enunciazione della poetica acmeista.[^3] È in questa nuova prospettiva che nacque la traduzione di **Michail Lozinskij**, divenuta poi la traduzione ‘classica’ della _Commedia_, letta e studiata dai russi ancora oggi.

Il saggio _Conversazione su Dante_ (1933) di **Mandel’štam** rappresenta infine un tentativo di eliminare _“tutte le stratificazioni romantiche e neoromantiche accumulatesi [...] attorno alla figura e all’opera del fiorentino”_.[^4] Criticando l’atteggiamento della sua patria nei confronti del sommo poeta, Mandel’štam contrappone al mito del Dante aristocratico la figura di un goffo e insicuro Dante _raznočinec_ (ovvero un intellettuale che non apparteneva alla nobiltà), che tuttavia era un vero “artigiano della parola”:

>Dante è un poveraccio di antico sangue romano; interiormente è un raznočinec, meno capace di compiutezza che del suo opposto. Bisognerebbe essere talpe cieche per non vedere che in tutta la Divina Commedia egli non sa come comportarsi, dove mettere i piedi, non sa cosa deve dire, né come fare un inchino. [...] Gli studi danteschi sono sempre stati ostacolati dalla fama del Poeta, e continueranno probabilmente a esserne ostacolati ancora per molto tempo. La 'lapidarietà' di Dante è semplicemente l'effetto di un grandissimo squilibrio psicologico cui il Poeta trova sfogo immaginando supplizi, sognando incontri.

L'immagine proposta da Mandel’štam è dunque ben lontana da quella consueta dell’esule genio, la cui aura mistica aveva di fatto impedito di intraprendere studi più approfonditi sulla persona e l'opera di Dante Alighieri.

### Dante socialista

Se all’inizio del Novecento gli studi accademici su Dante lo vedevano essenzialmente come un autore cristiano legato alla tradizione cattolica medievale, a partire dagli anni Venti iniziarono a comparire nuovi studi che non solo riportarono Dante nel contesto della lotta contro l’autorità del Papa, ma che soprattutto diedero un’**interpretazione marxista** alla _Divina Commedia_. Per riportare un paio di esempi, lo storico Vadim Bystrjanskij paragonava la lotta contro il Papa alla lotta della classe operaia contro il capitalismo; Anatolij Lunačarskij guardava la _Divina Commedia_ come a un’opera politico-sociale, considerandola la prima al mondo ad aver
analizzato e definito la classe borghese. Nel 1935, inoltre, il presidente dell’Unione degli scrittori sovietici Aleksandr Fadeev definì Dante persino progenitore del metodo del realismo socialista,[^5] riconoscendo alla _Commedia_ il pregio di aver fornito un quadro oggettivo della lotta di classe del suo tempo nonostante l’allegoria del viaggio fantastico nell’Oltretomba.

### Dante profeta

Totalmente opposta era invece l’interpretazione che di Dante fece l’**_intelligencija_ emigrata** dopo la Rivoluzione. Fu in particolare **Dmitrij Merežkovskij**, fuggiasco e condannato a morte dai bolscevichi, a vedere nel sommo poeta il portavoce di una particolare interpretazione del Cristianesimo, l'uomo che aveva compreso meglio di chiunque altro il significato del proprio tempo. L'esule russo sentiva il proprio destino molto vicino a quello di Dante, tant’è che nell’introduzione della sua monografia _Dante_ (1937) scrisse: _“È solo un caso o qualcosa di più, il fatto che, proprio in questi giorni penosi per tutta l’umanità, [...] un russo scriva di Dante, un mendico di un mendico, un disprezzato, un esule di [un] esule, un condannato a morte di un condannato a morte?”_.[^6]

Secondo Merežkovskij, l’eccezionalità di Dante stava nella lotta per un **Cristianesimo futuro** e nell’annunciazione di una nuova esistenza per l’umanità, cose per cui aveva lottato anche egli stesso nel suo Paese. Nelle tre cantiche della _Commedia_ Merežkovskij vedeva allora il cammino di iniziazione dell’essere umano verso il **Regno del Terzo Testamento**, ovvero il Regno dello Spirito Santo che sarebbe venuto con la riconciliazione dell'umanità nella Trinità:

>Il simbolo della guerra è il Due. Due classi nemiche, i ricchi e i poveri, nell’economia; due popoli, il proprio e lo straniero, nella politica; [...] due Dei, l’uomo e Dio, nella religione. Ovunque è il Due, e fra i Due arde senza fine la guerra. Perché la guerra cessi, bisogna che i Due si uniscano nel Terzo: le due classi in un popolo, i due popoli dell’universalità, [...] le due religioni, l’umana e la divina, in quella divino-umana. [...] Il simbolo matematico della pace è dunque il Tre.

Sebbene non sia stato molto apprezzato dai contemporanei, il libro di Merežkovskij offre un interessante ritratto psicologico di Dante, cosa che non era mai stata fatta fino ad allora: un poeta scosso da una grande pena d’amore perché sposato a una donna che non amava e che, preso da “deliri di follia” dopo la morte dell'amata Beatrice, era stato in grado di creare la più grande opera della letteratura mondiale.

## Gli autori "danteschi"

Come abbiamo visto, l’opera dantesca trovò terreno fertile nella civiltà letteraria russa e ispirò, tra l’Ottocento e il Novecento, opere come _Le anime morte_ (1842) di Nikolaj Gogol’, la trilogia _L’ebreo arriva_ (1888-1891) di Vsevolod Krestovskij, _Canto d'inferno_ (1909) di Aleksandr Blok, _Dante_ (1937) di Dmitrji Merežkovskij, _L'ultimo Cerchio (e il Nuovo Dante all'Inferno)_ (1942-1944) di Z. N. Gippius e molte altre ancora. Il sistema concettuale dantesco ebbe inoltre grande influenza sulle concezioni estetiche e poetiche dell’Acmeismo, come nella poesia di Anna Achmatova, Nikolaj Gumilëv od Osip Mandel’štam.

### Nikolaj Gogol' (1809-1852)

Precursore del realismo magico, Gogol' era un grandissimo scrittore di racconti, dove la memoria storica della Russia s'intreccia alla magia delle fiabe. Nelle sue opere Gogol' contrappone al passato glorioso dell'Impero russo un presente meschino e opprimente, ben rappresentato dalla burocrazia di San Pietroburgo. _Le anime morte_ (1842), definito dall'autore un "poema", potrebbe essere definita una "Divina Commedia russa", che aveva l'ambizione di mostrare un affresco della Russia contadina e zarista &mdash; denunciandone la mediocrità e il materialismo &mdash; e la smisurata ricchezza dello spirito russo attraverso la redenzione del protagonista Čičikov.

"Anime morte" era l'espressione con la quale la burocrazia russa si riferiva ai servi della gleba deceduti che però risultavano ancora vivi in attesa del nuovo censimento e per i quali i proprietari terrieri pagavano ancora le tasse. Tuttavia il titolo non si riferisce soltanto ai servi, ma anche ai loro padroni, abbruttiti e grotteschi, che non mostrano alcuna traccia di vita spirituale. Il romanzo si componeva originariamente di due parti: la prima aveva lo scopo di rappresentare l'"inferno" del presente, mentre la seconda avrebbe dovuto proporre figure edificanti per avviare un percorso di redenzione per l'intero popolo russo. La realtà della Russia zarista offriva però ben poco materiale per la seconda parte dell'opera e nel 1845 Gogol' bruciò quasi tutti i materiali della seconda parte.

### Aleksandr Blok (1880-1921)

Tra i poeti simbolisti cultori dell’Eterno Femminino, Blok fu senza dubbio il più importante, il cui ciclo di poesie sulla Bellissima Dama (1904) gli fece guadagnare la fama di “poeta dantesco”. Se le poesie sulla Bellissima Dama si erano ispirate alla _Vita nova_ (così come a Solov’ëv, Goethe e Petrarca), dopo una crisi spirituale del poeta che lo fece sentire abbandonato dalla sua Musa, la _Divina Commedia_ divenne la nuova fonte d’ispirazione per interpretare l’“inferno” della realtà e del mondo moderno. Frutto di questa nuova ispirazione è la raccolta _Poesie. Libro terzo_ (1907-1916), dove è contenuta la poesia in terzine **_Canto d’inferno_ (1909)**.

### Dmitrij Merežkovskij (1866-1941)

Ideatore della società filosofico-religiosa nella città di San Pietroburgo ("Incontri Religioso-Filosofici di San Pietroburgo") e della rivista _La nuova strada_ (1902-1904), Merežkovskij fu costretto a lasciare la Russia clandestinamente nel 1919, a causa dei complicati rapporti con lo Stato e la Chiesa russi. Non abbandonando mai la speranza di un ritorno in patria, egli vedeva la salvezza per il mondo intero nella sua **teoria del Terzo Testamento**, di cui, insieme a Dante Alighieri, si considerava il profeta. Con la sua opera **_Dante_ (1937)** Merežkovskij voleva allora riportare in vita il profeta che aveva mostrato per primo la via della salvezza, la via d’uscita dalla selva oscura. Il Dante di Merežkovskij si fece così portavoce dei migranti russi, fuggiti, come lui, da una patria che li voleva morti ma verso la quale erano ancora rivolti i sospiri. Quest’opera rappresenta difatti un tentativo dell’autore di risvegliare le coscienze
per incitarle alla lotta contro l’Unione Sovietica, da lui ritenuta il "Regno dell’Anticristo".

Al di là degli elementi biografici in comune con Dante, quasi tutta l’opera di Merežkovskij è profondamente legata alla civiltà trecentesca; inoltre, la trilogia _Cristo e Anticristo_ (1896-1905) &mdash; che nel 1931 gli valse la candidatura al Premio Nobel per la letteratura &mdash; era molto vicina alla _Divina Commedia_ per tematiche e struttura. Se dovessimo fare un confronto tra Dante e Merežkovskij, troveremmo un punto d’incontro sicuramente nella concezione del mondo per contrasti, secondo un complesso sistema di tesi, antitesi, sintesi e schemi trinitari.

### Z. N. Gippius (1869-1945)

Celebre _salonnière_ a San Pietroburgo e Parigi, Gippius era una delle figure centrali del Simbolismo russo e, insieme a Merežkovskij, del risveglio religioso dell’_intelligencija_ russa. Nel loro misticismo, raffinatezza linguistica, sofisticatezza e
umorismo, le storie di Gippius somigliano molto alle novelle medievali; inoltre le analisi introspettive dei suoi personaggi descrivono quella che riteneva sarebbe dovuta essere l’umanità del Terzo Testamento.

Durante gli ultimi anni della sua vita Gippius scrisse il poemetto **_L'ultimo Cerchio (e il Nuovo Dante all'Inferno)_ (1942-1944)**, legato sia all’idealismo, sia al dantismo russo. Scritto sulla falsa riga della _Divina Commedia_, il poemetto rappresenta un esperimento poetico in cui si è cercato di creare sia un Dante “moderno” molto vicino all'interpretazione di Merežkovskij, sia l'immagine di un inferno molto vicina al _Canto d'inferno_ di Blok.

---

## Bibliografia

- Achunzjanova, Farida (2009). “Roman D. S. Merežkovskogo ‘Dante’ v
kul’turologičeskom kontekste russkoj emigracii”, _Intelligencija i mir_, 2: 120-124.
- Bystrjanskij, Vadim (1921). “Pamjati Dante (k 600-letiju so dnja smerti)”, _Kniga i revoljucija_, 1: 13-16.
- Caprioglio, Nadia, Spendel, Giovanna (1989). “Dmitrij Merežkovskij e Dante Alighieri”, in Guidubaldi a cura di 1989, I: 341-350.
- Cifariello, Alessandro (2009). “Riflessioni Dantesche di Michele Colucci”, _Dante. Rivista Internazionale di Studi su Dante Alighieri_, IV: 213-222.
- Colucci, Michele (1989). “Un Dante russo nel nostro secolo”, in Guidubaldi a cura di 1989, I: 351-359.
- Colucci, Michele (2007). _Tra Dante e Majakovskij. Saggi di letterature comparate slavo-romanze_, Roma: Carocci editore.
- Cornwell, Neil, eds (1998). _Reference Guide to Russian Literature_, Chicago and London: Fitzroy Dearborn Publishers.
- Fadeev, Aleksandr (1935). “C”ezd sovetskich pisatelej i socialističeskaja kul’tura”, _Na rubeže_, 2: 74-86.
- Goleniščev-Kutuzov, Il’ja (1971). _Tvorčestvo Dante i mirovaja kul’tura_, pod redakcej V. Žirmunskogo, Moskva: Akademija Nauk SSSR.
- Grebneva, Marina (2011). “Chudožestvennaja sostavljajuščaja mifa o florentijskom poete Dante Alig’eri v russkoj literature XIX-XX vekov”, _Filologija i čelovek_, 4: 139-146.
- Guidubaldi, Egidio S. J. a cura di (1989). _Dantismo russo e cornice europea_, I, Firenze: Leo S. Olschki Editore.
- Ivanov Vjačeslav (2016). "Ivanov Vjač. Čistilišče, I, 1-67", in Samarina et alia sost. 2011: 840-842.
- Katenin, Pavel (1965). _Izbrannye proizvedenija_, Moskva-Leningrad: Sovetskij pisatel'.
- Landa, Kristina (2020). _“Božestvennaja Komedija” v zerkalach russkich perevodov. K istorii recepcii dantovskogo tvorčestva v Rossii_, Sankt-Peterburg: Izdatel’stvo RCHGA.
- Mandel’štam, Osip (2021). _Discorso su Dante_, tr. it. di M. Olsoufieva, Milano: SE Srl.
- Merežkovskij, Dmitrij (1938). _Dante_, trad. it. di R. Küfferle, Bologna: Zanichelli.
- Pachmuss, Temira (1998). “Zinaida Nikolaevna Hippius”, in Cornwell eds 1998: 383-387.
- Polonskij, Vadim (2015). “Russkij Dante konca XIX ‒ pervoj poloviny XX v: opyty recepcii i interpretacii klassiki do i posle revoljucionnogo poroga”, _Literaturovedčeskij žurnal_, 37: 111-130.
- Salmon, Laura (1989). “Tra specchi ed ombre: il Dante di Blok e di Bal’mont. Studi per una traduzione”, in Guidubaldi a cura di 1989, I: 239-264.
- Samarina, Marina et alia sost. (2016). _Dante: pro et contra. Ličnost' i nasledie Dante v ocenke russkich myslitelej, pisatelej, issledovatelej: antologija_, Sankt-Peterburg: Izdatel'stvo RCHGA.

## Sitografia

- Carpi, Guido (2005). <a href='https://www.treccani.it/enciclopedia/nikolaj-vasilevic-gogol_(Enciclopedia-dei-ragazzi)/' target='_blank'>"Gogol', Nikolaj Vasil'evič"</a>, in _Enciclopedia dei ragazzi_.

---
## Note

[^1]: Lo slavo ecclesiastico era la lingua liturgica della Chiesa ortodossa dell'area slava, come quella russa, bulgara, macedone o serba. Nonostante non venisse mai parlata al di fuori delle funzioni religiose, prima del XVIII secolo in Russia lo slavo ecclesiastico era la lingua letteraria.
[^2]: I decabristi erano membri di società segrete che diffondevano idee liberali e socialiste nella Russia zarista del XIX secolo. Quello decabrista fu il primo movimento rivoluzionario russo a chiedere l'abolizione della servitù della gleba e la fondazione di uno stato repubblicano.
[^3]: L'Acmeismo è un movimento letterario nato in contrapposizione al Simbolismo. Il suo stile espressivo si fonda sulla chiarezza del linguaggio e sulla concretezza dei contenuti. 
[^4]: Colucci 2007: 184.
[^5]: Movimento artistico e culturale dell'Unione Sovietica che celebrava il progresso socialista. Temi ricorrenti erano la lotta di classe, la storia del movimento operaio, la vita quotidiana dei lavoratori.
[^6]: Traduzione di R. Küfferle.