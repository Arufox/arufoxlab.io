---
title: "La neuroermeneutica. Un approccio innovativo alla letteratura tra scienze umanistiche e neuroscienze"
date: 2024-01-06T16:35:51+02:00
featured: false
description: "La neuroermeneutica s’inserisce nell’attuale dibattito transdisciplinare tra studi letterari, cognitivi e neuroscientifici, soprattutto per quanto riguarda il funzionamento dell’attività mentale durante la lettura. La sua innovazione è proprio nell’indagine condivisa tra scienze umanistiche, cognitive e neuroscienze. Nonostante alcuni limiti e criticità, la neuroermeneutica può rivelarsi utile per comprendere quei processi della mente umana ancor oggi misteriosi e, di conseguenza, giungere a una più profonda conoscenza dell’essere umano."
tags:
- neuroermeneutica
- letteratura
- neuroscienze
- approccio neuroermeneutico
- critica letteraria
- studi sul cervello
toc: true
image: "/img/articoli/leggere.jpg"
imageCaption: "Foto di <a href='https://unsplash.com/it/@elcarito' target='_blank'>Carlos Torres</a>"
---

---

>Per noi tutto consiste nel nostro concetto del mondo. Modificare il nostro concetto del mondo significa modificare il mondo riguardo a noi, significa in altre parole modificare il mondo, perché il mondo non sarà mai per noi altra cosa se non quello che è per noi.

<p style="text-align: center; font-size: 0.9em; padding-bottom: 70px">F. PESSOA, <em>Il libro dell’inquietudine di Bernardo Soares</em>.</p>

## Introduzione

Nei secoli gli studiosi di letteratura hanno elaborato tante teorie, metodologie e approcci per interpretare il testo letterario. Ciò perché l'**essere umano** è il fondamento della letteratura e, in quanto creatura di questa terra, egli si evolve e muta, così come muta il modo con cui egli interpreta il mondo e crea significati. In quanto creazione umana anche la letteratura deve essere intesa come qualcosa in continuo divenire. Per questo motivo non verrà mai trovata una teoria o un metodo definitivi per lo studio e l’interpretazione del testo letterario: le **norme** che permettono di definirne le caratteristiche sono difatti **socialmente costruite** e non solo derivano da un **processo di acculturazione ed educazione**, ma sono anche soggette al **condizionamento storico**.

A cominciare dall’atteggiamento ostile nei confronti del **senso** inaugurato da Nietzsche, tante sono state le teorie letterarie sviluppate nel corso dei decenni. Tuttavia, una svolta importante si ebbe tra la fine degli anni Settanta e l'inizio degli anni Ottanta, quando gli studi storico-letterari iniziarono a interessarsi, sulla scia delle teorie di Foucault, ai nuovi modi di scrivere la storia e, in particolar modo, al **concetto di immaginario**. Il testo letterario iniziava allora a essere concepito come sistema complesso e pluristratificato, e venivano messe in discussione persino le principali posizioni critiche attorno a un autore, un testo, un problema &mdash; fenomeno che Ricoeur definì _“conflitto delle interpretazioni”._

L’obiettivo dei nuovi studi letterari è analizzare la **nuova ricezione** e le **nuove interpretazioni** delle opere del passato, nella convinzione che i diversi modi di approcciare un testo ne trasformino progressivamente la fisionomia, cambiandone il significato e il valore. Ma se esistono infinite prospettive di interpretazione di un’opera letteraria, allora come verranno interpretate oggi e in futuro le opere del passato?

## Il contributo delle neuroscienze negli studi letterari

Recenti studi nati dalla collaborazione tra scienze umanistiche, neuroscienze e scienze cognitive hanno dimostrato come la **cognizione** e la **percezione** funzionino per mezzo di una relazione molto soggettiva tra mente, corpo e ambiente in un dato contesto biologico-culturale. Inoltre, è stato osservato empiricamente che tali dinamiche sono implicate anche nell’**atto della lettura**: quando si legge un testo letterario, quest’ultimo stimola la mente e il corpo di chi legge, che ne _costruisce_ (e non _ri-costruisce_) il senso a partire da tali stimolazioni. Durante la lettura, infatti, la mente viene guidata dagli elementi del testo letterario a creare un **mondo “virtuale”**, spesso controfattuale: tale processo coinvolge l’intero **background del lettore** (memorie, cultura d’appartenenza, identità di genere, estrazione sociale, orientamento sessuale ecc.), nonché la sua **sfera fisica, emotiva e immaginativa**.

Lettura e conoscenza vengono dunque concepite come esperienze radicate profondamente nel **corpo del soggetto** in una complessa unità motoria, percettiva ed emotiva. Così come la realtà viene "costruita" attraverso la percezione dei cinque sensi, allo stesso modo il significato del testo letterario viene _costruito_ dal lettore a seconda delle vibrazioni provenienti dal **suo mondo interiore**. Per questo motivo studiare il testo e la relazione con il lettore significa studiare anche le **convenzioni di lettura**, le quali sono influenzate dalla **storia**, dalla **cultura** e dal **linguaggio**. Nell'atto della lettura non c'è inoltre differenza tra **lettore comune** e **specialista**, in quanto il godimento dell’esperienza estetica avviene indipendentemente dal fatto che chi legge abbia o meno alle spalle una formazione accademica.


## La "neuro-svolta" degli studi letterari

Recentemente gli studi letterari hanno ridefinito i propri paradigmi euristici per indagare quei processi cognitivi implicati nell’esperienza della lettura. Nonostante non sia ancora chiaro cosa avvenga nella mente mentre si legge, quel che emerge dagli esperimenti empirici è che la **lettura** è un importante **strumento evolutivo umano** tramite il quale possono essere sviluppate conoscenze e capacità sociali, relazionali e di previsione. Tra i processi cognitivi che si attivano durante la lettura, il più importante è senza dubbio l'**empatia**, la quale è al centro di un’ampia discussione filosofica e pedagogica sul suo ruolo nello sviluppo del **senso morale**, della giustizia sociale e del comportamento prosociale.

Responsabile di tali cambiamenti è ciò che viene definita la “neuro-svolta” (_neuro-turn_) degli studi letterari e cognitivi, la quale ha condotto gli studi verso la definizione di una _"epistemologia basata sul cervello”_ (Edelman 2007) che lega insieme gli studi delle **scienze umanistiche**, delle **scienze cognitive** e delle **neuroscienze** per studiare il modo in cui la mente immagina mondi fittizi, costruisce nuovi significati.

### L'embodied cognition

Questi nuovi studi si devono a Varela e Muratana (1985), i quali concepiscono la conoscenza come un’**esperienza incarnata**, aprendo la strada all’**embodied cognition**. Con quest'ultimo termine ci si riferisce ai **processi cognitivi** che mettono in relazione **mente, corpo e ambiente** e che &mdash; secondo Kosslyn e Koenig (1992) &mdash; sono attivati dai meccanismi automatici dell’omeostasi biologica. In questo modo il **sentimento cosciente del Sé** viene inteso come un’entità in grado di sentire, ricordare e **narrare** la propria esperienza nel mondo.

L’origine delle emozioni, del pensiero, della coscienza e della percezione sono stati studiati con i moderni strumenti di mappatura del cervello &mdash; risonanza magnetica funzionale, tomografia a emissione di positroni, magnetoencefalografia, stimolazione magnetica transcranica &mdash; e si è scoperto che l’**attività mentale** (_mindbrain_) è profondamente radicata nella **corporeità** in una complessa **unità motoria, percettiva ed emotiva.** Ciò significa che l’essere umano non acquisisce le informazioni in modo passivo, bensì attua un processo di costruzione del senso in base alla relazione, molto personale, tra mente, corpo e ambiente. Allora la **cognizione** dipenderebbe dalle esperienze derivate dall’avere un **corpo** con specifiche capacità percettive e motorie, inquadrate in un determinato **contesto biologico-culturale**.

### L'intelletto incarnato

Trova così conferma empirica la teoria dell’**intelletto incarnato** che Merleau-Ponty aveva postulato solo nel 1945. Secondo il filosofo francese la realtà non verrebbe colta come “dato”, ma verrebbe piuttosto "costruita" dal **percettore** secondo la propria percezione, secondo un _pattern_ scaturito dalla sua **specifica identità**. Il soggetto di questa esperienza non è l’intelletto puro, bensì l’**intelletto incarnato**, che abita il tempo e lo spazio dei corpi non come _“io penso”_, ma come _“io faccio”_ le cose nel mondo.

Inoltre, gli studi di Zeki (1993), Turner (1996) e poi un esperimento del 2013 (Berns _et al._ 2013) hanno dimostrato che non ci sono differenze tra le **strutture neuronali** attivate di fronte a esperienze reali (vista e udito) e quelle attivate mentre si legge. Accade persino che le percezioni attivate durante lo stato di “immersione” in mondi finzionali siano più potenti rispetto a quelle attivate nel mondo reale. Fludernik (2013) chiama **esperienzialità** quella capacità del testo di evocare un’esperienza nella mente del lettore, e viene attivata attraverso dei **processi di rispecchiamento** che permettono di vivere mentalmente le vicende narrate. Il cosiddetto **trasporto narrativo** è un’esperienza d’immersione totale nella **dimensione diegetica**, in seguito alla quale la percezione del lettore si allontana dal mondo reale e si “immerge” nella storia.

### La lettura come attività incarnata

La **lettura** deve essere allora concepita come **attività incarnata**, ovvero legata alla **sfera corporea di chi legge.** Ne consegue che le nuove teorie di interpretazione del testo letterario non devono più considerare la struttura di quest'ultimo quale unico oggetto dell’indagine. L’**esperienza letteraria** dovrebbe essere valutata come un processo complesso in cui da una struttura linguistico-formale si arriva a una grande varietà di **risposte fisiologiche ed emotive** da parte di lettrici e lettori (**teoria della ricezione** [Iser 1976, Jauss 1977, Ricoeur 1986]). 

Viene così introdotta la **dimensione “virtuale” del testo letterario**, ovvero il mondo immaginativo ed emotivo evocato in chi legge: sebbene l’esperienza di lettura sia individuale e unica, ciascuno riesce a costruire il senso delle storie proprio perché appartengono a un **repertorio di sequenze narrative ricorrenti** che poi sono state rielaborate dalla mente di chi legge per collimare con la propria **esperienza personale**. Partendo da questi presupposti, la **neuroermeneutica** si propone di indagare il testo letterario attraverso una **prospettiva orientata al lettore** in collaborazione con le **neuroscienze**.

## La neuroermeneutica

Sulla base di quanto è stato detto finora, la **neuroermeneutica** (Gambino & Pulvirenti 2018) s’inserisce nell’attuale dibattito transdisciplinare tra studi letterari, cognitivi e neuroscientifici, soprattutto per quanto riguarda il **funzionamento dell’attività mentale durante la lettura**. Basata sulla **teoria ermeneutica di Schleiermacher** e dell’**embodied cognition**, la neuroermeneutica adotta una prospettiva ancorata al **corpo** che guarda all’opera letteraria come il risultato di continue e complesse interazioni tra mente, corpo, ambiente ed epoca storica.

L’investigazione neuroermeneutica cerca di spiegare la **rilevanza antropologica del processo di costruzione del senso**, in quanto sia il processo narrativo sia la capacità di creare storie riescono a plasmare la mente umana, la memoria e il modo di essere-nel-mondo dell’essere umano. È proprio la capacità di **proiettare la propria esperienza in un mondo immaginativo** &mdash; virtuale &mdash; ad aver permesso all'essere umano di sviluppare le sue peculiari capacità di pensiero, previsione e simulazione empatica con l’altro.

L'**approccio neuroermeneutico** (Gambino & Pulvirenti 2018) si pone l’obiettivo di interpretare da un lato i prodotti culturali quali **dispositivi della conoscenza umana** &mdash; dunque ascritti a una dimensione storico-culturale &mdash; mentre dall’altro studia l’**interazione lettore-testo** attraverso una **prospettiva incarnata** che tiene in considerazione il _background_ dei lettori “comuni” (memorie, cultura d’appartenenza, identità di genere, estrazione sociale ecc.), i loro corpi e il coinvolgimento della loro sfera fisica, emotiva e immaginativa.

Si tratta di un **approccio costruttivista** perché parte dal presupposto che il **senso di un testo** debba essere sempre _costruito_ dai lettori, attingendo pertanto alla **teoria della ricezione** (Iser 1976, Jauss 1977, Ricoeur 1986). Tale teoria concepisce i **lettori** come **riceventi attivi** in grado di mettere in discussione l’immanenza del testo, inaugurando il passaggio dal lettore “tipico” concepito in modo astratto al lettore “prototipico” e lo inserisce in una **media statistica studiabile**.

### L'approccio neuroermeneutico

L’approccio neuroermeneutico si propone quale superamento dell’attuale contrapposizione fra **approccio cognitivo** &mdash; empirico, ignora le peculiarità testuali &mdash; e **approccio ermeneutico** &mdash; teorico, si basa sull’interpretazione dello specialista. A partire da una **dinamica circolare** &mdash; il cosiddetto **circolo neuroermeneutico** (Gambino & Pulvirenti 2018) &mdash; la neuroermeneutica intende cogliere le molteplici variabili di natura fisiologica, storico-culturale, antropologica, filosofica ecc. attraverso una **prospettiva transdisciplinare**. Tali variabili costituiscono il _background_ dei lettori e interagiscono tra loro nella _costruzione_ dei **fenomeni mentali** stimolati da uno specifico testo, il quale viene trattato come **fonte inesauribile di significati** che può essere interrogata e interpretata **infinite volte.**

Riprendendo il concetto del sospetto di Ricoeur, la **neuroermeneutica del sospetto** (Gambino & Pulvirenti 2019b) indaga il modo in cui il lettore costruisce il significato di un testo letterario grazie agli “indizi” che emergono dal suo **foregrounding linguistico, stilistico e retorico**, i quali attivano in chi legge una **reazione fisiologica e cognitiva** (stati emotivi, processi memoriali, processi immaginativi ecc.). A livello linguistico il “sospetto” emergerebbe dall’**ambiguità del segno linguistico**, che induce un’interpretazione doppia, indiretta, in quanto le strutture testuali poste in essere dal _foregrounding_ aumentano il potere evocativo e creativo della lingua.

L'innovazione di tale approccio è nel superamento degli approcci tradizionali alla letteratura, troppo orientati al _source text_ e incentrati sulla scomposizione del testo nelle sue componenti formali e storico-culturali. Si elencano qui di seguito gli **assunti fondamentali dell’approccio neuroermeneutico** (Gambino & Pulvirenti 2018):

- Il **significato** è un fenomeno che emerge dall’interazione fra testo, autore e lettore.
- Il **testo** è espressione del pensiero dell’autore.
- Il **lettore** viene concepito come una sorta di “autore di secondo grado”, in quanto costruisce il mondo della narrazione nella propria mente.
- Il **circolo neuroermeneutico** si attiva tramite quattro coordinate: **foregrounding** e **backgrounding**, la dinamica del **blending** e l’atto di **unblending**;
- L’**atto interpretativo** nato dalla lettura è **inesauribile**, poiché un testo può essere letto infinite volte, innescando altrettanto infiniti atti interpretativi.
- Nell’atto interpretativo viene superata la distinzione tra lettore “comune” e lettore “specialista”.
- Esiste una differenza tra la lettura di un **testo letterario** e quella di un **testo non letterario**. Le strategie di significazione sono specifiche per ciascuna tipologia testuale e possono essere evidenziate tramite la comparazione dello scarto tra linguaggio letterario e linguaggio comune.

<p></p>

### Il metodo di analisi neuroermeneutica

Come abbiamo visto, il circolo neuroermeneutico studia il **rapporto autore-opera-lettore** in una dinamica circolare in cui il lettore &mdash; che è portatore del proprio bagaglio culturale &mdash; incontra il pensiero dell'autore attraverso il **linguaggio formalizzato dell'opera letteraria**. Da questo incontro il lettore ne uscirà cambiato, in quanto la lettura ne avrà ampliato l'**orizzonte cognitivo**.

Il circolo ermeneutico funziona secondo quattro coordinate (Gambino & Pulvirenti 2018):
- **blending**: processo mentale che permette all'**autore** di creare significati a livello preconscio o inconscio a partire dalla propria esperienza personale;
- **foregrounding**: peculiarità formali, strutturali e stilistiche del **testo letterario**; sono metarappresentazioni del pensiero dell'autore che il lettore può attivare mediante il proprio _background_ cognitivo;
- **backgrounding**: contesto storico-culturale di testo, autore e lettore;
- **unblending**: atto tramite il quale il **lettore** _crea_ il significato degli elementi del _foregrounding_ nella propria mente, subendo inoltre l'influenza del proprio _background_ personale. Tramite l'_unblending_ il lettore produce l'evento narrato nella sua immaginazione, immaginando se stesso a farne esperienza.

 L'interpretazione del testo presume difatti una selezione tra elementi posti in primo piano (_foreground_) rispetto a uno sfondo (_background_).

Secondo il metodo di analisi neuroermeneutica, i testi letterari andrebbero letti monitorando le risposte sensoriali ed emozionali scaturite dalla lettura. Inoltre è necessario analizzare gli **elementi di foregrounding e backgrounding**, i quali &mdash; come suggerisce Jacobs (2015) &mdash; possono essere descritti a livello metrico, fonologico, sintattico e semantico attraverso lo schema **matrice 4x4 di Roman Jakobson**. Secondo tale schema, a ogni livello corrisponderebbero proprietà sublessicali, lessicali, interlessicali e sopralessicali che provocherebbero una risposta emozionale in chi legge. Come suggeriscono Battaglia & Mignemi (2019), tale risposta può essere infine interpretata, ad esempio, attraverso il **modello Plutchik**, che distingue quattro coppie di emozioni primarie le cui combinazioni generano emozioni più complesse. A questo punto un interessante spunto di riflessione potrebbe essere: _in che modo il linguaggio suscita determinate emozioni nel lettore? Quali fattori facilitano l'immersione nella narrazione?_

{{< figure
    src="/img/articoli/matrice.png"
    attr="Schema della matrice 4x4 di Roman Jakobson, tratta da <a href='https://www.researchgate.net/figure/4-4-matrix-illustrating-four-levels-of-text-crossed-with-four-groups-of-features-with_fig5_273768005' target='_blank'>Jacobs Arthur M. (2015)</a> su licenza <a href='https://creativecommons.org/licenses/by/4.0/' target='_blank'>Creative Commons Attribution 4.0 International</a>."
>}}

---


L'**immersione narrativa** è un processo di simulazione mentale e incarnata che consente al lettore di essere "trasportato" nel mondo finzionale. Solitamente i lettori si identificano con personaggi a loro vicini (stessa età, genere, gruppo etnico ecc.), tuttavia la lettura allena a provare empatia anche con i personaggi più diversi. È possibile distinguere tre tipi di immersione (Florio & Biancifiore 2019):

- **immersione spazio-temporale**: risposta all'ambientazione e alla voce del narratore;
- **immersione temporale**: risposta alla trama; genera _suspense_, si desidera sapere come andrà a finire la storia;
- **immersione emozionale**: risposta ai personaggi; genera empatia, catarsi, ci fa mettere nei panni dell'altro (_perspective and role taking_).

La mente del lettore è inoltre sempre impegnata a valutare moralmente le azioni dei personaggi. La **ricerca della giustizia** aumenta i livelli di ossitocina, considerato l'ormone dell'empatia che incrementa la tendenza alla moralità, alla generosità e la passione per la storia (nonché quella per il _brand_) (Calabrese 2016). Allora leggere modificherebbe l'equilibrio neurochimico dei lettori e la loro predisposizione verso individui e situazioni.

## Criticità

L'approccio neuroermeneutico non è immune a criticità e problematiche dal punto di vista della sua applicazione. Nonostante la base teorica risulti ben sviluppata e affascinante, non sembra tuttavia possibile applicarla senza cadere nelle vecchie abitudini che analizzano unicamente il testo, le intenzioni dell'autore e il suo contesto storico. Pertanto come dovrebbe approcciarsi lo **studioso di neuroermeneutica**, quindi uno specialista, all'analisi di un testo? Forse come uno scienziato, sospendendo il proprio giudizio e raccogliendo i dati sulle sensazioni di lettrici e lettori. I lettori, e quindi i sondaggi e gli esperimenti, sarebbero allora imprescindibili da questo tipo di analisi del testo.

Tuttavia la domanda sorge ora spontanea: che ne sarebbe della critica letteraria e dell'arte stessa se la valutazione e l'apprezzamento di un'opera venissero appiattiti dalla fredda analisi statistica? Che ne sarebbe degli specialisti di letteratura? Verrebbero anch'essi schiacciati dagli indici di gradimento del pubblico, il quale apprezza ciò che incontra il proprio gusto?

## Conclusioni

Fino alla fine del secolo scorso gli **approcci tradizionali** degli studi letterari tendevano a frammentare i testi nelle loro componenti formali e storico-culturali secondo una **prospettiva
decostruzionista**. Tuttavia, in termini di interpretazione, tali approcci sono ancora troppo orientati al **source text** e all’interpretazione del **lettore “specialista”**.

Venuta meno la differenza tra lettore “specialista” e lettore “comune” ai fini dell’interpretazione letteraria, l'adeguatezza di un **approccio orientato al lettore** è proprio nell’attenzione posta sul **lettore “comune”**, il quale fruisce un’opera letteraria per il piacere della lettura e la interpreta secondo la sua personale visione del mondo. In questo modo, uno stesso testo si apre a infinite possibilità di interpretazione perché non smetterà mai di essere letto da **futuri lettori**, ognuno con una propria differente percezione e visione del mondo.

L’innovazione dell’interpretazione neuroermeneutica del testo letterario è proprio nell’indagine condivisa tra scienze umanistiche, cognitive e neuroscienze. Nonostante alcuni limiti e criticità, la neuroermeneutica può rivelarsi utile per comprendere quei processi della mente umana ancor oggi misteriosi e, di conseguenza, giungere a una più profonda conoscenza dell’essere umano (Gambino & Pulvirenti 2018). Tale approccio può essere inoltre adottato anche per studiare i prodotti culturali degli altri _media_, come ad esempio film, videogiochi, canzoni, audiolibri; così come può rivelarsi utile per una riflessione ambientalista sullo sfruttamento della natura e delle sue risorse.

---

## Bibliografia

- Baumgarten, Alexander G. (1750). _Metaphysica_, Magdeburg: Impensis Carol Herman Hemmerde.
- Berns, Gregory S., Blaine, Kristina et al. (2013). "Short- and Long-Term Effects of a Novel on Connectivity in the Brain", _Brain Connectivity_, 3, 6: 590-600.
- Calabrese, Stefano a cura di (2016). _Narrare al tempo della globalizzazione_, Roma: Carocci editore.
- Capucci, Luigi (1994). _Il corpo tecnologico. L’influenza delle tecnologie sul corpo e le sue facoltà_, Bologna: Baskerville.
- Edelman, Gerald M. (2007). _Seconda natura. Scienza del cervello e conoscenza umana_, tr. it. S. Frediani, Milano: Raffaello Cortina Editore.
- Fludernik, Monika (2013). "Verso una narratologia ‘naturale’", tr. it. F. Pennacchio, _Enthymema_, 8: 141-191.
- Galimberti, Umberto (2021). _Il corpo_, Milano: Feltrinelli.
- Gallese, Vittorio (2004). "Embodied Simulation: From Neurons to Phenomenal Experience", _Phenomenology and the Cognitive Sciences_, 4 (1): 23-48.
- Gambino, Renata, Pulvirenti, Grazia (2018). _Storie menti mondi. Approccio neuroermeneutico alla letteratura_, Udine e Milano: Mimesis.
- Gambino, Renata, Pulvirenti, Grazia (2019a). "Neurohermeneutics. A Transdisciplinary Approach to Literature", _Gestalt Theory_, 14, 2: 185-200.
- Gambino, Renata, Pulvirenti, Grazia (2019b). "The Neurohermeneutics of Suspicion. A Theoretical Approach", _Comparatismi_, 4: 144-163.
- Giovannetti, Paolo (2008). _La letteratura italiana moderna e contemporanea_, Roma: Carrocci.
- Giovannetti, Paolo (2012). _Il racconto. Letteratura, cinema, televisione_, Roma: Carrocci.
- Iser, Wolfgang (1976). _Der Akt des Lesens: Theorie ästhetischer Wirkung_, München: Fink Verlag.
- Jacobs, Arthur M. (2015). "Neurocognitive Poetics: methods and models for investigating the neuronal and cognitive- affective bases of literature reception", _Frontiers in Human Neuroscience_, 9, 186: 1-22.
- Jauss, Hans R. (1977). _Ästhetische Erfahrung und literarische Hermeneutik_, Frankfurt a.M: Suhrkamp.
- Johnson, Mark (1987). _The Body in the Mind: The Bodily Basis of Meaning, Imagination, and Reason_, Chicago: Chicago University Press.
- Kosslyn, Stephen M., Koenig, Oliver M. (1992). _Wet Mind. The New Cognitive Neuroscience_, New York: The Free Press.
- Merleau-Ponty, Maurice (1972). _Fenomenologia della percezione_, tr. it. A. Bonomi, Milano: il Saggiatore.
- Miall, David S., Kuiken, Don (1998). "The Form of Reading: Empirical Studies of Literariness", _Poetics_, 25: 327-341.
- Monod, Jacques (1970). _Il caso e la necessità_, tr. it. A. Busi, Milano: Mondadori.
- Pessoa, Fernando (2019). _Il libro dell’inquietudine di Bernardo Soares_, tr. it. M. José de Lancastre e A. Tabucchi, Milano: Feltrinelli.
- Ricoeur, Paul (1970). _Freud and Philosophy: An Essay on Interpretation_, tr. ingl. D. Savage, London: Yale University Press.
- Ricoeur, Paul (1974). _The Conflict of Interpretations. Essays in Hermeneutics_, edited by D. Ihde, Evanston and Illinois: Northwestern University Press.
- Ricoeur, Paul (1986). _Tempo e racconto I_, tr. it. G. Grampa, Milano: Jaca Book.
- Schleiermacher, Friedrich (1838). "Hermeneutik und Kritik", in Id., _Sämmtliche Werke_, Berlin: Reimer: 5-262.
- Turner, Mark (1996). _The Literary Mind_, New York: Oxford University Press.
- Varela, Francisco J., Muratana, Humberto R. (1985). _Autopoiesi e cognizione. La realizzazione del vivente_, Venezia: Marsilio.
- Varela Francisco J., Thompson, Evan, et al. edited by (1991), _The Embodied Mind. Cognitive Science and Human Experience_, Cambridge: MIT Press.
- Varela, Francisco J. (1994). “Il reincanto del concreto”, in Capucci a cura di 1994: 143-159.
- Zeki, Semir (1993). _A Vision of the Brain_, Oxford and New York: Oxford University Press.

## Sitografia

- Florio, Biancifiore (2019). <a href='http://www.neurohumanitiestudies.eu/wp-content/uploads/2019/05/LINNAMORAMENTO-LETTERARIO-Storie-damore-in-Dante-Goethe-e-Boccaccio.pdf' target='_blank'>"L’innamoramento letterario: storie d’amore in Dante, Boccaccio e Goethe"</a>.
- Mazzaglia, Piera, Mignemi, Federica (2019). <a href='http://www.neurohumanitiestudies.eu/wp-content/uploads/2019/05/Emozioni-Mazzaglia-Mignemi.pptx' target='_blank'>"Emozioni"</a>.
