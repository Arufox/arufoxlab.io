---
title: "Papà Goriot. Un'analisi"
date: 2022-04-02T20:08:14+02:00
description: Tra le opere di Balzac, Papà Goriot (1834) è il romanzo che meglio analizza i meccanismi della corruzione della società e della violenza delle passioni. Questo articolo fornisce un'analisi dettagliata del romanzo.
featured: false
tags:
- balzac
- letteratura francese
- parigi
- borghesia
- naturalismo
- critica della società
- analisi

toc: true
image: "/img/articoli/goriot.jpg"
imageCaption: "_The Empress Eugénie Receiving the Diplomatic Corps after the Birth of the Imperial Prince_ (1856), disegno di Constantin Guys. Fonte: <a href='https://www.clevelandart.org/art/collection/search' target='_blank'>The Cleveland Museum of Art</a>."
---

---

Honoré de Balzac (1799-1850) può essere comparato a un demiurgo, ovvero un dio creatore. In vent'anni ha difatti prodotto un'opera gigantesca, _La Comédie humaine_, composta da ben 137 opere e 2209 personaggi.

## _La commedia umana_ (1831-1850)

Balzac ebbe la grande ambizione letteraria di rappresentare un affresco della società della propria epoca, di descriverne le differenti _specie_ sociali e umane. Come scrive nella prefazione alla _Comédie humaine_, tale idea ebbe origine proprio da un confronto tra umanità e animalità:

> La Società somigliava alla natura. La Società non fa forse dell'uomo, a seconda degli ambiti nei quali la sua azione si dispiega, tanti uomini differenti di quante sono le specie della zoologia? Le differenze tra un soldato, un operaio, un amministratore [...] sono altrettanto degne di considerazione di quelle che contraddistinguono il lupo, il leone, l'asino [...]. Come esistono le Specie zoologiche, sono dunque sempre esistite, e sempre esisteranno, le Specie Sociali.

Il titolo è ambizioso poiché fa esplicitamente riferimento alla
_Divina Commedia_ di Dante Alighieri, ma con la differenza che qui è l'essere umano a essere posto al centro dell'edificio balzacchiano. Tuttavia, con il termine "Comédie" Balzac fa anche riferimento al teatro, che secondo lui altro non è che il mondo stesso, nel quale ciascun individuo gioca il proprio ruolo; le cui regole del gioco delle false sembianze garantiscono ai suoi partecipanti la riuscita sociale. L'ambizione di Balzac è dunque quella di creare un romanzo che piaccia al poeta, al filosofo e alle masse e che permetta di capire la _"ragione del movimento della società"_.

La struttura dell'opera è basata sul sistema di ritorno dei personaggi e divide i testi in tre cicli: **studi di costume** (che comprendono scene di vita privata, parigina, politica, militare, di campagna e i parenti poveri), **studi filosofici** e **studi analitici**. Gli **studi di costume** &mdash; che è il ciclo cui appartiene _Papà Goriot_ &mdash; costituiscono un grande affresco della società francese, un vero documento sulla Restaurazione e sulla monarchia di Juillet, periodo durante il quale si assiste alla nascita della società capitalistica e del suo nuovo dio: il denaro.

I personaggi di Balzac non sono soltanto dei **tipi umani**, ma
anche dei **tipi sociali**, poiché per lui ci sono tante specie quante sono le classi sociali. La parola "specie" viene utilizzata dall'autore proprio come farebbe un naturalista quando parla di animali, ed è inoltre convinto che l'essere umano sia determinato dall'ambiente in cui vive. Le lunghe descrizioni dell'ambiente permettono difatti a chi legge di inserire il personaggio in un _milieu_ ben definito. Inoltre, i personaggi sono anche dei **tipi psicologici**, in quanto le loro passioni si spingono fino all'estremo e possono essere facilmente riconosciuti. Il compito del romanziere diventa allora trovare quella situazione di crisi in cui il personaggio può esprimersi in tutta la sua verità.

Balzac procede molto per intuizione e immaginazione. Basandosi sulla documentazione e l'osservazione, egli dà consistenza ai suoi personaggi secondo un'intuizione "visionaria" e ne estremizza le passioni più forti, come l'avarizia, l'ambizione, la crudeltà, il cinismo. È per questo motivo che quello di Balzac viene chiamato "realismo visionario". Molto innovativo è anche lo stile narrativo della _Comédie_. A differenza degli altri romanzi dell'epoca che utilizzano l'io, Balzac ricorre a un narratore onnisciente che conduce la trama, spiega e commenta la situazione, anticipa quello che accadrà dopo, manifesta le proprie riflessioni.

## _Papà Goriot_ (1834)

Con il romanzo _Le père Goriot_ Balzac voleva creare una _maîtrise d'œuvre_ e riesce nel suo intento, dato che è la sua
opera più conosciuta. Appartiene al gruppo delle scene di vita privata e descrive i meccanismi della corruzione della società e la violenza delle passioni. Come scrive Balzac stesso, nel dramma che verrà a consumarsi nella vicenda narrata _"all is true"_, ovvero tutto è talmente vero da permettere a ognuno di noi di riconoscere la condizione umana presentata e il suo ambiente. È così per mezzo del protagonista Eugène de Rastignac che è possibile esplorare diversi _tranches de vie_ parigini, dalla miseria della pensione in cui egli risiede fino alla spregevole alta società parigina cui aspira con fervore.

### Parigi

La città moderna &mdash; dunque la Parigi del XIX secolo &mdash; rappresenta la sovrastruttura del romanzo. La città moderna (a differenza di quella postmoderna) è un sistema di forze inquietanti e misteriose, ma ancora _leggibile_. Il sistema dei personaggi-tipo e quello urbano sono inscindibili, in quanto l'uno richiama costantemente l'altro. La città non può essere compresa se non attraverso la sua analisi economica e la trama principale del romanzo è **accumulare denaro**. Balzac scrive all'alba del sistema capitalistico, ma è già un maestro nel mettere in relazione classi sociali e flussi di denaro ed è il primo a parlarne con una tale precisione. Quando Balzac racconta del modo con cui si accumula un capitale, rivela che dietro vi si celano fatti irraccontabili. Nella Parigi del XIX secolo il successo è tutto, è la chiave del potere; le leggi e la
morale sono impotenti davanti ai ricchi, poiché la ricchezza è
virtù. Un sentimento non viene perdonato nella sua pienezza
così come a un uomo non viene perdonato di non avere un soldo:
tutto è una cambiale economica, persino i sentimenti e l'amore. Un uomo viene amato fintanto che ha soldi da investire nel proprio amore; un padre è amato finché ha denaro da dare ai figli.

Nell'opera Balzac mette subito in luce le due principali **complessità** caratteristiche della città moderna: la complessità del sovraccarico sensoriale e la complessità di un sistema caotico che si auto-organizza.

Nell'idea del **sovraccarico sensoriale**, la città spinge al limite il sistema nervoso, stimolando una serie di riflessioni e sviluppando quelli che sono i nuovi valori estetici del caos: il rumore e l'insensatezza vengono così trasformati in un'esperienza estetica che abbraccia l'anarchia e fa poesia del tutto. Le metafore del carro della civiltà e dell'idolo di Jagernatt sono quelle che meglio esprimono la condizione dei cittadini che, vittime di un sovraccarico sia sensoriale sia razionale, creano una società _blasé_, cioè apatica, cinica e indifferente ai drammi che vi si consumano, in quanto il carro della civiltà _"continua il suo cammino glorioso"_.

La città è una **complessità caotica e paradossale** che sovrasta l'individuo, ha una personalità propria e si organizza in base a milioni di decisioni individuali e incontri casuali. Si tratta dunque di un ordine globale che nasce a partire da interazioni individuali. Ogni sistema urbano sarà dunque diverso l'uno dall'altro ed è per questo motivo che l'autore si chiede se la storia drammatica originatasi nel "sistema complesso Parigi" potrà essere capita e compresa anche altrove:

> Le particolarità di una vicenda ricca di osservazioni e di colore locale non possono essere apprezzate se non tra le colline di Montmartre e le alture di Montrouge, nell'illustre vallata di calcinacci sempre sul punto di cadere e di rigagnoli neri di fango; vallata piena di sofferenze reali, di gioie spesso ingannevoli, e così frenetica che occorre un qualcosa di esorbitante per crearvi una sensazione di una certa durata.

### La pensione Vauquer

Auerbach definì la descrizione della pensione Vauquer il capolavoro assoluto del "realismo atmosferico" di Balzac. È situata nel Quartier Latin, a sud di Parigi, ed è un quartiere malfamato abitato dal popolino che arriva dalla provincia. Come tutte le grandi città, anche Parigi ha uno o più quartieri destinati ai poveri, i quali avevano la funzione di allontanare e nascondere la povertà agli occhi dei ricchi. Tale concetto è reso ancora più forte quando l'autore ci parla del significato che un luogo così orribile può assumere agli occhi dei parigini: c'è chi vi vedrà soltanto un luogo fatiscente, ma c'è anche chi vi vedrà una casa.

L'ambiente è descritto nei minimi dettagli, dettagli che rendono un quadro preciso del _milieu_ in cui sono collocati i personaggi. A differenza di quanto avrebbe fatto Gustave Flaubert, che aveva un'ossessione per i dettagli, persino per quelli più irrilevanti, a un certo punto del racconto Balzac si rende conto di non poter rallentare ulteriormente la narrazione con le sue descrizioni. In casa Vauquer si possono osservare gli elementi di una società completa, con i suoi tipi sociali e psicologici: l'avaro, il borghese decaduto, il forzato, l'artista, la zitella, il buono a nulla, lo studente giovane e ambizioso, l'ereditiera.

**La signora Vauquer** è una donna di grandi ambizioni; è frugale, avara, pusillanime e diffidente come una gatta.
La sua ambizione era di _"tornare in pista"_, di fare
l'arrampicata sociale servendosi di Goriot, ma sentendosi
rifiutata da lui, non ha potuto fare a meno di diffamarlo secondo quel meccanismo psicologico che fa attribuire alla vittima le stesse meschinità del colpevole. La signora ha anche la particolarità di non fidarsi degli amici ma di confidarsi con gli estranei:

> Un tipo di comportamento strano ma vero, di cui è facile trovare le radici nel cuore umano. Può darsi che alcuni non abbiano più niente da guadagnare dalle persone con cui vivono; dopo aver mostrato loro il vuoto della propria anima, se ne sentono giudicate con meritata severità. Provando però un invincibile bisogno di adulazioni che non ricevono, o divorati dalla voglia di apparire dotati di qualità che non possiedono, sperano di carpire la stima o il cuore degli estranei, rischiando prima o poi di perderli.

Queste 'qualità' fanno della signora Vauquer una creatura meschina, falsa e detestabile; è la tipica donna anziana avvezza a fare supposizioni su ogni cosa e che passa in chiacchiere le proprie serate.

**Vautrin** è un personaggio sadico e manipolatore, un vero _Übermensch_. Conosce tutto, è sorridente e servizievole ma, malgrado l'aria bonaria, suscita molto timore poiché sembra quel tipo di persona che rimarrebbe imperturbata anche davanti a un delitto. Il suo sguardo sembra indagare a fondo ogni problema, coscienza e sentimento; sa o indovina le vicende di ognuno, ma nessuno è in grado di intuire i suoi pensieri e le sue occupazioni. Ciononostante, a volte lascia scorgere la spaventosa profondità del suo carattere, rivelando il suo piacere nel sottomettere e infliggere dolore all'altro. È uno dei personaggi più importanti dell'opera perché gestisce la trama del romanzo, ed è il più complesso della _Comédie humaine_: malvagio e avvolto nel mistero, Vautrin non è collocabile socialmente e detesta le gerarchie sociali, inoltre rappresenta i misteri di una situazione spaventosa accuratamente nascosta da chi ne è l'artefice. È un uomo superiore che non obbedisce a niente e sta al di sopra delle leggi, che considera soprattutto ingiuste perché condannano gli assassini ma non le persone infide, che ritiene ben peggiori.

**Jean-Joaquim Goriot** è l'opposto di Vautrin. Ex pastaio, è lo
zimbello della pensione, poiché dopo aver fatto fortuna nella carestia dopo la sollevazione del 1789, da ricco borghese che era stato si è ridotto alla miseria. È intelligente soltanto nei suoi affari da commerciante, ma fuori dalla sua specialità torna a essere un rozzo e ottuso, incapace di capire un ragionamento semplice. Rimasto vedovo, aveva sviluppato il sentimento paterno fino all'insensatezza, viziando le figlie e amando tutto di loro, persino il male che gli infliggono. In questo senso Goriot rappresenta il "Cristo della paternità", ovvero un padre che morirebbe e patirebbe le pene dell'inferno per la felicità delle sue figlie.

**Eugène de Rastignac** è un giovane studente di legge sulle cui
spalle grava il peso delle speranze dei genitori. Spirito sagace
e ambizioso di fare il proprio debutto in società. È colpito dalla
_vague des passions_, quello stato d'animo che precede il pieno sviluppo delle passioni tipico dei giovani oziosi e chiusi in loro stessi, i cui desideri non riescono a esaurirsi in nessun oggetto. Rastignac è dotato di grande immaginazione e desidera per sé una vita elegante e piena di piaceri, piuttosto che una caratterizzata da privazioni.
> Quando uno studente senza soldi afferra un briciolo di
piacere vi si attacca come un cane all'osso.

Dopo aver conosciuto il lusso, Rastignac inizia a spendere tutto il denaro dei genitori per mantenere uno stile di vita raffinato (_"gli attrezzi con cui si zappa la vigna"_) e mantenere salve le apparenze. Eppure dopo aver fatto il suo debutto in società, Rastignac comincia a comprenderne l'insania e sente la propria anima dilaniata tra desiderio di ricchezza e senso della morale e della responsabilità. In questo modo, egli veicola una visione antropologica pessimista: in un misto di intelligenza, sensibilità e debolezza Rastignac rappresenta il tipo psicologico dell'uomo piccolo-borghese; la sua intelligenza gli permette di capire il mondo ma non di opporvisi, mostrando la più grande remissività morale.

### L'educazione di Rastignac

Con un rapporto caratterizzato da tenerezza e sadismo, Rastignac viene "educato alla vita" da Vautrin. Vautrin ha difatti compreso i meccanismi e le regole del mondo moderno e tutto quel che insegna a Rastignac è improntato del mito dell'uomo metropolitano: gli parla difatti di Benvenuto Cellini (tema della realizzazione delle ambizioni dell'individuo nonostante le avversioni e le delusioni), guarda se stesso come alla torre nel gioco degli scacchi, insegna al ragazzo che bisogna giocare da soli contro tutti gli altri per avere successo. Come si raggiunge il successo? Serve **denaro**. Il denaro è qui elevato a valore morale, pertanto il valore di un essere umano viene calcolato in termini economici. Può essere guadagnato onestamente, cioè lavorando sottostando a leggi e regole, il che significa dover battere la concorrenza in abilità e sottomissione a un superiore; oppure può essere guadagnato battendo gli avversari in astuzia, ovvero accaparrandosi la dote migliore o persino uccidendo, se necessario. Non ha importanza il mezzo dell'arrampicata sociale, purché sia un processo eseguito con furbizia e accortezza, poiché a vincere sarà solamente il migliore.

Se la vita è regolata dal **caso** e dall'**azzardo**, bisogna dunque affrontarla come una scommessa. In questa dimensione caotica dell'esperienza, l'esistenza di un individuo ha senso soltanto se ha successo in società. Secondo Vautrin, se il mondo avesse leggi fisse allora non cambierebbe mai, pertanto sono soltanto i fatti e le circostanze a regolarlo e l'uomo superiore è colui che sposa i fatti per dirigere le circostanze: un grande calcolatore. _"L'uomo non è tenuto ad essere più saggio di tutta una nazione"_, ma solo a essere più saggio di un altro uomo. Tutto va fatto per tornaconto personale e ciò che i moralisti chiamano "abissi del cuore umano" altro non sono che i moti dell'interesse personale; persino i ripensamenti sono soltanto calcoli che mirano al proprio tornaconto.

Vautrin si interessa a Rastignac perché può trarre vantaggio dalla situazione e gli propone uno scambio di favori. Il giovane studente è continuamente tentato da quell'uomo e si sente come un burattino nelle sue mani, pronto a fare qualunque cosa gli ordini. Ad esempio, tacendo vigliaccamente alla proposta di Vautrin, acconsente di fatto all'omicidio del signor Taillefer. Il piano di Vautrin di sposare l'ereditiera Victorine Taillefer tenta spesso il povero Rastignac, ma egli è innamorato della baronessa Delphine de Nucingen, la secondogenita di Goriot. Ciononostante, anche il successo amoroso è dovuto al caso e la condizione che Delphine impone al ragazzo ne è il simbolo: deve scommettere al casinò per dimostrare il suo amore per lei. Così Rastignac tenta al gioco così come tenta al debutto in società: proprio come quando si tenta la fortuna con i numeri per il denaro, al casinò egli tenta la fortuna per il suo amore. Sarà dunque la roulette a decidere la sua sorte con Delphine.

### La monetizzazione dei sentimenti

Rastignac intuisce che per arrivare al vertice della macchina
sociale deve aggrapparsi a un ingranaggio. Vedendo in Goriot il giusto trampolino di lancio verso l'alta società e la vita di ricchezze che desidera, Rastignac tesse così una trama di interessi in un triangolo tra lui, Dolphine e Goriot. Fa amicizia con Goriot e diventa il suo prediletto. Inoltre, sapendo della rivalità tra le sorelle Goriot e conoscendo la brama di potere di Delphine, Rastignac sfrutta la propria parentela con la viscontessa de Beauseant per ammaliare la ragazza con la promessa del prestigio sociale, suscitando in lei un interesse opportunistico che la fa legare ancora di più.

Alla fine del romanzo Restignac riceve l'ultima grande lezione proprio da Delphine. Egli era pronto a sacrificare la propria coscienza all'amante, la quale lo aveva influenzato con le proprie sregolatezze, ne aveva fatto sbiadire la famiglia e si era appropriata di ogni cosa a proprio vantaggio. La loro passione era nata grazie al godimento reciproco e si amavano per le voluttà concesse. La natura sensibile di Rastignac gli permette di capire l'animo di Delphine, la quale calpesterebbe persino il cadavere del padre pur di fare la sua arrampicata sociale, eppure non ha il coraggio di farle la morale.
Se prima Rastignac desiderava debuttare in società, chiedendo alla
famiglia un sacrificio economico enorme, alla fine comprende la sofferenza di un dramma familiare consumato dal denaro. Nonostante ormai disprezzi e giudichi moralmente Delphine alla morte di Goriot, Rastignac non le si oppone: _"aveva imparato ad amare egoisticamente"_.

In punto di morte Goriot comprende la vera natura delle sue figlie. Con la toccante frase _"bisogna morire per sapere che cosa
sono i figli"_, Goriot mostra di aver appreso una dura lezione, cioè che quando è uno sventurato a essere amato, si può essere sicuri di quell'amore. Quando Goriot era ricco tutti lo trattavano con riguardo, ma quando è caduto in miseria è stato abbandonato da tutti e condannato a morire solo. Nonostante sul letto di morte Goriot maledice le figlie per tutto il male che gli hanno fatto, un padre rimane comunque un padre e l'amore paterno è più grande dell'odio, così, prima di spirare, le perdona.

Il **denaro** è dunque uno dei più grandi protagonisti dell'opera. Goriot rappresenta una visione arcaica del denaro, pertanto _deve_ morire: la morte di Goriot rappresenta infatti il passaggio da un sistema economico vecchio a uno nuovo, quello capitalistico.

### Il dominio del caso

Il dominio del caso è rappresentato dall'arresto di Vautrin: il caso è padrone del mondo e la sfortuna e l'insuccesso travolgono chiunque, anche l'_Übermensch_. L'arresto di Vautrin è descritto come uno spettacolo orrendo e maestoso, come una scena teatrale. In questo momento Vautrin (il cui vero nome è Jacques Collin, detto "Trompe-la-Mort") mostra la sua natura, la sua grandezza e bassezza, diventa il simbolo di una nazione degenerata, di un popolo selvaggio ed estremamente logico, brutale, cinico e arrendevole. Durante l'arresto esprime tutti i sentimenti umani, fuorché il pentimento: non oppone resistenza alla polizia ma assicura a Rastignac che manterrà i suoi accordi con lui. Così il giovane studente accetta quella "parentela" criminale come espiazione dei suoi cattivi pensieri. Scontratosi con il sangue freddo di Vautrin, l'egoismo di Delphine, il senso del dovere e il valore della fortuna fatta con le proprie fatiche, Rastignac si fa più forte, tuttavia la società giocherà sempre un ruolo corruttivo su di lui: queste lezioni di vita gli hanno insegnato che in un mondo dominato dal caso l'onore è un valore inutile e d'intralcio.

## Conclusioni

Ne _Le père Goriot_ Honoré de Balzac descrive una Parigi la cui gerarchia sociale è stabilita dall'ammontare del capitale. Tale sistema è descritto come se fosse un ordine naturale, come se stesse alla base della naturale distinzione tra le 'razze' (il nobile, il borghese, il povero ecc...) del genere umano, proprio come si fa con gli animali. La gerarchia sociale è un sistema che soffoca la coscienza, annichilisce le persone e finisce per adattarle a sé come una vite in una macchina.

> Ciò che i poveretti non farebbero nel loro interesse, lo compiono non appena sentono 'Sua Eccellenza'.

Nonostante le passioni siano spinte all'estremo, non si tratta
comunque di personaggi piatti o grotteschi, ma di personaggi complessi, con una propria psicologia, i propri conflitti interiori. Essi possono essere difatti considerati modelli esplicativi delle varie condizioni umane della società parigina ottocentesca.

---

## Bibliografia

- Honoré de Balzac (2015). _Papà Goriot_. Trad. it. di E. Klersy Imberciadori, Milano: Garzanti.