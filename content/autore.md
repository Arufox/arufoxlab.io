---
title: "Alessandra Gallia"
toc: false
nosearch: true
norss: true
showDate: false
showTitle: false
sharemenu: false
---

<div style="display: block">
<p style="position: absolute">
<img src="/img/autore/sfondo.jpg" alt="vecchio libro di fiabe" float="left" width="980px">
</p>
<p style="position: relative; padding-top: 37%; float: left; padding-left: 15%">
<img src="/img/autore/ritratto.png" alt="una volpe con gli occhiali" width="210px">
</p>
<h1 style="position: relative; text-align: center; font-size: 1.2em; padding-top: 56%; margin-bottom: -35px;">{{<slogan content="Alessandra Gallia">}}</h1>
<p style="text-align: right; padding-right: 10%"><em>Dott. in Letterature moderne, comparate e postcoloniali</em></p>
</div>

<p style="text-align: left; padding-left: 10%; padding-right: 10%; margin-top: 60px; margin-bottom: 70px;">
<strong>Aka Arufox</strong><br>
Classe 1996, di Siracusa.<br>
Insegno, traduco, scrivo per blog e faccio fumetti per passione. Il mio sogno è visitare l'Asia... Poi certo, mi piacerebbe vivere grazie alla mia arte.</p>

{{< mquote content="Agisci soltanto secondo quella massima che, al tempo stesso, puoi volere che divenga una legge universale." author="Immanuel Kant" >}}

<h2 style="text-align: left; padding-top: 50px; padding-left: 10%; margin-bottom: 50px;">Pubblicazioni</h2>
<p style="text-align: justify; padding-left: 10%; padding-bottom: 20px; padding-right: 10%">Alessandra Gallia, Chiara Casale (2024). <a href="https://www.amazon.it/Leftland-Alessandra-Gallia/dp/B0CYP3Q923/" target="_blank"><em>Leftland (graphic novel)</em></a>, Lecce: Youcanprint.</p>
<p style="text-align: justify; padding-left: 10%; padding-bottom: 20px; padding-right: 10%">Z. N. Gippius (2023). <a href="https://www.amazon.it/Lultimo-Cerchio-Nuovo-Dante-allInferno/dp/B0C4NBGJV6/ref" target="_blank"><em>L'ultimo Cerchio e il Nuovo Dante all'Inferno</em></a>, a cura di Alessandra Gallia, Lecce: Youcanprint.</p>
<p style="text-align: justify; padding-left: 10%; padding-bottom: 20px; padding-right: 10%">Alessandra Gallia (2023). <a href="https://www.enbypost.it/2023/04/29/non-binarismo-nella-poesia-russa-di-fine-secolo-il-caso-di-z-n-gippius/" target="_blank">"Non binarismo nella poesia russa di fine secolo: il caso di Z. N. Gippius"</a>, <em>Enbypost. Non binary online magazine.</em></p>
<p style="text-align: justify; padding-left: 10%; padding-bottom: 20px; padding-right: 10%">Alessandra Gallia (2023). <a href="https://www.enbypost.it/2023/03/08/steve-walker-e-la-tenerezza-dellarte-gay/" target="_blank">"Steve Walker e la tenerezza dell’arte gay"</a>, <em>Enbypost. Non binary online magazine.</em></p>
<p style="text-align: justify; padding-left: 10%; padding-bottom: 80px; padding-right: 10%">Alessandra Gallia (2023). <a href="https://www.enbypost.it/2023/08/20/la-sensualita-del-nudo-maschile-nei-dipinti-di-nebojsa-zdravkovic/" target="_blank">"La sensualità del nudo maschile nei dipinti di Nebojsa Zdravković"</a>, <em>Enbypost. Non binary online magazine.</em></p>

---

<h2 style="text-align: center; margin-top: 70px; margin-bottom: 80px">Interessi</h2>

{{< imggrid
	img0="/img/autore/lettura.jpg"
	label0="Libri"
	img1="/img/autore/fotografia.jpg"
	label1="Fotografia"
	img2="/img/autore/traduzione.jpg"
	label2="Traduzione"
	img3="/img/autore/disegno.jpg"
	label3="Disegno"
	img4="/img/autore/videogioco.jpg"
	label4="Videogiochi"
>}}

<h2 style="text-align: center; margin-top: 100px; margin-bottom: 80px">Supportami</h2>

<p style="text-align: justify; padding-right: 10%; padding-left: 10%; margin-bottom: 30px">Se ti piace quel che scrivo e vuoi supportarmi nel mio lavoro, clicca sull'icona in basso per offrirmi una pizza. Inoltre con la prima donazione riceverai un regalo da parte mia! <a href="https://www.amazon.it/farfalla-dorata-Eva-Goldsby-ebook/dp/B0C74M95WM/" target="_blank">Qui</a> trovi anche un piccolo racconto tradotto da me.</p>

<p style="text-align: center; margin-top: 40px">Grazie! &#128150;</p>

<script data-name="BMC-Widget" data-cfasync="false" src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js" data-id="arufox" data-description="Support me on Buy me a coffee!" data-message="Se ti piace quel che scrivo e vuoi supportarmi nel mio lavoro, offrimi una pizza! È facile e non richiede un account." data-color="#FF813F" data-position="Right" data-x_margin="90" data-y_margin="18"></script>

<p style="font-size: 0.85em; margin-top: 140px;">Per l'immagine di copertina si ringrazia <a href="https://unsplash.com/it/@foxfox" target="_blank">Natalia Y</a>.<br>
Per le immagini della sezione "interessi" si ringraziano (in ordine) <a href="https://unsplash.com/@blazphoto" target="_blank">Blaz Photo</a>, <a href="https://unsplash.com/@andalexander" target="_blank">Andrew Draper</a>, <a href="https://unsplash.com/@florianklauer" target="_blank">Florian Klauer</a>, <a href="https://unsplash.com/@tumanova_photo" target="_blank">Daria Tumanova</a>, <a href="https://unsplash.com/@sorakhan" target="_blank">Sora Khan</a>.</p>
